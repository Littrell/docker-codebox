#!/bin/bash

port_exists () { 
    local array="$1[@]"
    local seeking=$2
    local in=1
    for element in "${!array}"; do
        if [[ $element == $seeking ]]; then
            in=0
            break
        fi
    done
    return $in
}

# Set the directory we're going to run Codebox in
workingdir=$(pwd)

# Move to Dockerfile directory
cd $(dirname "$0")

# Build image
docker build -t codebox:latest  .

# Gather all codebox containers
CONTAINERS=$(docker ps -aq)

# Loop over each gathering the external ports and storing them
EXTERNAL_PORTS=()
for container in $CONTAINERS
do
    ports=$(docker port $container)
    IFS=$'/' read -rd '' -a external_port<<<"$ports"
    EXTERNAL_PORTS+=(${external_port[0]})
done

if [ ${#EXTERNAL_PORTS} -eq 0 ]; then
    next_port=8080
else
    # Sort
    EXTERNAL_PORTS=($(sort <<<"${EXTERNAL_PORTS[*]}"))

    # Grab the next available port
    for port in $EXTERNAL_PORTS
    do
        next_port=$((port+1))
        # If the next port doesn't exist, use that
        if [ ! $(port_exists EXTERNAL_PORTS $next_port) ]; then
            break;
        fi
    done
fi

# Build container
#docker run --rm --name="codebox-$next_port" -p $next_port:8080 -v $workingdir:/workspace -P codebox -p 8080 /workspace --open
docker run --rm -p $next_port:$next_port -v $workingdir:/workspace --name="codebox-$next_port" -i -t codebox:latest codebox run . -p $next_port

cd $workingdir
