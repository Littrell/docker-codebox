FROM node:0.10

RUN set -x \
    && sed -i 's/# \(.*multiverse$\)/\1/g' /etc/apt/sources.list \
    && apt-get update \
    && apt-get -y install git \
    && npm -g install codebox

WORKDIR /workspace
