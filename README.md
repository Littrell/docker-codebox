# Description
docker-codebox is a Docker container that serves the current directory and all of its files in a collaborative IDE by volume mounting and utilizing [CodeboxIDE] (https://github.com/CodeboxIDE/codebox).   

# Setup

After cloning, apply the following alias
```
alias codebox="/bin/bash <path to repository directory>/build.sh"
```

[build.sh] (https://gitlab.com/Littrell/docker-codebox/blob/master/build.sh) is a helper script used to setup volume mounting and ports.   

If you would rather use Docker directly, use the following:
```shell
docker build -t codebox:latest .

docker run -p <port>:<port> -v $PWD:/workspace --name="codebox-<port>" -i -t codebox:latest codebox run . -p <port>
```